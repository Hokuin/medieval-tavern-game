﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TotalResult : MonoBehaviour
{
    public DiceResult[] resultsPlayer1;
    public DiceResult[] resultsPlayer2;

    /// <summary>
    /// Player1's score got during current round.
    /// </summary>
    public int scorePlayer1;
    
    /// <summary>
    /// Player1's score got during current round.
    /// </summary>
    public int scorePlayer2;
    
    /// <summary>
    /// Stores a value of each type of unit ("blanks", archers, horsemen, pikemen, shielders, swordsmen).
    /// </summary>
    public int[] typeToPoints = new int[6] { 0, 2, 4, 2, 3, 3 };

    /// <summary>
    /// Stores extra amounts of points for number of the same units (two, three, four, five, six or seven same units
    /// </summary>
    public int[] extraPointsForUnitCollection = new int[6] { 7, 10, 15, 25, 40, 60 };
    
    private void Awake()
    {
        scorePlayer1 = 0;
        scorePlayer2 = 0;
    }

    private void Update()
    {
        Vector2Int scores = ComputeTotalScore(resultsPlayer1, resultsPlayer2);
        scorePlayer1 = scores.x;
        scorePlayer2 = scores.y;
    }

    public int[] CountUnits(DiceResult[] _result)
    {
        int[] numberOfUnits = new int[6] { 0, 0, 0, 0, 0, 0 };

        foreach (DiceResult dR in _result)
        {
            switch (dR.type)
            {
                case DiceResult.UnitType._blank: numberOfUnits[0]++; break;
                case DiceResult.UnitType.archer: numberOfUnits[1]++; break;
                case DiceResult.UnitType.horseman: numberOfUnits[2]++; break;
                case DiceResult.UnitType.pikeman: numberOfUnits[3]++; break;
                case DiceResult.UnitType.shielder: numberOfUnits[4]++; break;
                case DiceResult.UnitType.swordsman: numberOfUnits[5]++; break;
            }
        }

        return numberOfUnits;
    }

    public Vector2Int ComputeTotalScore(DiceResult[] _result1, DiceResult[] _result2)
    {
        Vector2Int totalScore = new Vector2Int();

        int[] numberOfUnitsPlayer1 = CountUnits(_result1);
        int[] numberOfUnitsPlayer2 = CountUnits(_result2);

        //basic points for types of units
        for (int i = 0; i < numberOfUnitsPlayer1.Length; i++)
        {
            totalScore.x += numberOfUnitsPlayer1[i] * typeToPoints[i];
            totalScore.y += numberOfUnitsPlayer2[i] * typeToPoints[i];
        }

        //extra points for collections of UNITS (excluding blanks)
        for (int i = 1; i < numberOfUnitsPlayer1.Length; i++)
        {
            switch (numberOfUnitsPlayer1[i])
            {
                case 2:
                    totalScore.x += extraPointsForUnitCollection[0];
                    break;
                case 3:
                    totalScore.x += extraPointsForUnitCollection[1];
                    break;
                case 4:
                    totalScore.x += extraPointsForUnitCollection[2];
                    break;
                case 5:
                    totalScore.x += extraPointsForUnitCollection[3];
                    break;
                case 6:
                    totalScore.x += extraPointsForUnitCollection[4];
                    break;
                case 7:
                    totalScore.x += extraPointsForUnitCollection[5];
                    break;
                default:
                    break;
            }

            switch (numberOfUnitsPlayer2[i])
            {
                case 2:
                    totalScore.y += extraPointsForUnitCollection[0];
                    break;
                case 3:
                    totalScore.y += extraPointsForUnitCollection[1];
                    break;
                case 4:
                    totalScore.y += extraPointsForUnitCollection[2];
                    break;
                case 5:
                    totalScore.y += extraPointsForUnitCollection[3];
                    break;
                case 6:
                    totalScore.y += extraPointsForUnitCollection[4];
                    break;
                case 7:
                    totalScore.y += extraPointsForUnitCollection[5];
                    break;
                default:
                    break;
            }
        }

        /* extra points for countering enemy's units
        
                     X ---> Y        X counters Y, so X gives extra points.
                     Extra = valueOfX + valueOfY for each found pair (1 unit can counter only 1 unit)

                           .--->shielder--->archer--->pikeman---.
                           |                                    |
                           |                                    |
                           `-------swordsman<----horseman<------`
        
         "blanks", archers, horsemen, pikemen, shielders, swordsmen
           0         1        2         3         4          5
           
        so:
                           .---> 4 ---> 1 ---> 3 ---.
                           |                        |
                           |                        |
                           `----- 5 <----- 2 <------`
        */

        //Player1's shielders counter Player2's archers
        totalScore.x += (typeToPoints[4] + typeToPoints[1]) * Mathf.Min(numberOfUnitsPlayer1[4], numberOfUnitsPlayer2[1]);
        //Player2's shielders counter Player1's archers
        totalScore.y += (typeToPoints[4] + typeToPoints[1]) * Mathf.Min(numberOfUnitsPlayer2[4], numberOfUnitsPlayer1[1]);

        //Player1's archers counter Player2's pikemen
        totalScore.x += (typeToPoints[1] + typeToPoints[3]) * Mathf.Min(numberOfUnitsPlayer1[1], numberOfUnitsPlayer2[3]);
        //Player2's archers counter Player1's pikemen
        totalScore.y += (typeToPoints[1] + typeToPoints[3]) * Mathf.Min(numberOfUnitsPlayer2[1], numberOfUnitsPlayer1[3]);

        //Player1's pikemen counter Player2's horsemen
        totalScore.x += (typeToPoints[3] + typeToPoints[2]) * Mathf.Min(numberOfUnitsPlayer1[3], numberOfUnitsPlayer2[2]);
        //Player2's pikemen counter Player1's horsemen
        totalScore.y += (typeToPoints[3] + typeToPoints[2]) * Mathf.Min(numberOfUnitsPlayer2[3], numberOfUnitsPlayer1[2]);

        //Player1's horsemen counter Player2's swordsmen
        totalScore.x += (typeToPoints[2] + typeToPoints[5]) * Mathf.Min(numberOfUnitsPlayer1[2], numberOfUnitsPlayer2[5]);
        //Player2's horsemen counter Player1's swordsmen
        totalScore.y += (typeToPoints[2] + typeToPoints[5]) * Mathf.Min(numberOfUnitsPlayer2[2], numberOfUnitsPlayer1[5]);

        //Player1's swordsmen counter Player2's shielders
        totalScore.x += (typeToPoints[5] + typeToPoints[4]) * Mathf.Min(numberOfUnitsPlayer1[5], numberOfUnitsPlayer2[4]);
        //Player2's swordsmen counter Player1's shielders
        totalScore.y += (typeToPoints[5] + typeToPoints[4]) * Mathf.Min(numberOfUnitsPlayer2[5], numberOfUnitsPlayer1[4]);

        return totalScore;
    }
}
