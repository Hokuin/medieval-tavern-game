﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class UserInterfaceManager : MonoBehaviour
{
    private static UserInterfaceManager _instance;
    /// <summary>
    /// An instance of a User Interface Manager singleton.
    /// </summary>
    public static UserInterfaceManager Instance { get { return _instance; } }

    [Header("Main Canvas")]
    public Canvas mainMenuCanvas;
    public Canvas playerInterfaceCanvas;

    [Header("Main menu buttons")]
    public Button startButton;
    public Button rulesButton;
    public Button settingsButton;
    public Button exitButton;

    [Header("Main menu exit confirmation")]
    public Image exitPanel;
    public Button exitButtonYes;
    public Button exitButtonNo;

    [Header("A panel with rules visualization")]
    public Image rulesPanel;
    public Button returnButtonFromRulesPanel;

    [Header("A panel with game settings")]
    public Image settingsPanel;
    public Slider musicSlider;
    public Slider effectsSlider;
    public Button returnButtonFromSettingsPanel;

    [Header("Heads Up Display")]
    public Button throwDicesButton;
    public Button returnButtonFromGame;

    [Header("Bet Buttons")]
    public Button betSmallButton;
    public Button betAverageButton;
    public Button betRiskyButton;

    [Header("Score & Gameplay Info")]
    public Text pocketPlayer1;
    public Text pocketPlayer2;
    public Text pointsPlayer1;
    public Text pointsPlayer2;
    public Text moneyInGame;
    public Text roundsLeft;

    [Header("Win & Lose screens")]
    public Image winImage;
    public Image loseImage;

    [Header("Audio")]
    public AudioSource soundtrackBackgroundSource;
    public AudioSource[] knockingDiceSources;
    public AudioSource knockingTestClip;

    private void Awake()
    {
        if (_instance != null && _instance != this) Destroy(gameObject);
        else _instance = this;
    }

    private void Start()
    {
        mainMenuCanvas.gameObject.SetActive(true);
        playerInterfaceCanvas.gameObject.SetActive(false);

        startButton.gameObject.SetActive(true);
        rulesButton.gameObject.SetActive(true);
        settingsButton.gameObject.SetActive(true);
        exitButton.gameObject.SetActive(true);

        exitPanel.gameObject.SetActive(false);
        exitButtonYes.gameObject.SetActive(true);
        exitButtonNo.gameObject.SetActive(true);

        rulesPanel.gameObject.SetActive(false);
        returnButtonFromRulesPanel.gameObject.SetActive(true);

        settingsPanel.gameObject.SetActive(false);
        returnButtonFromSettingsPanel.gameObject.SetActive(true);

        musicSlider.gameObject.SetActive(true);
        effectsSlider.gameObject.SetActive(true);
        
        betSmallButton.gameObject.SetActive(true);
        betAverageButton.gameObject.SetActive(true);
        betRiskyButton.gameObject.SetActive(true);

        throwDicesButton.gameObject.SetActive(false);
        returnButtonFromGame.gameObject.SetActive(true);

        pocketPlayer1.gameObject.SetActive(true);
        pocketPlayer2.gameObject.SetActive(true);
        moneyInGame.gameObject.SetActive(true);
        roundsLeft.gameObject.SetActive(true);
        pointsPlayer1.gameObject.SetActive(true);
        pointsPlayer2.gameObject.SetActive(true);

        winImage.gameObject.SetActive(false);
        loseImage.gameObject.SetActive(false);

        LoadSettings();
        soundtrackBackgroundSource.volume = musicSlider.value;
        for (int i = 0; i < knockingDiceSources.Length; i++)
        {
            knockingDiceSources[i].volume = 0.5f * effectsSlider.value / effectsSlider.maxValue;
        }

        InvokeRepeating("ImportGameData", 1f, 0.5f);
    }

    private void LoadSettings()
    {
        musicSlider.value = PlayerPrefs.GetFloat("Music volume");
        effectsSlider.value = PlayerPrefs.GetFloat("Effects volume");
    }

    private void ImportGameData()
    {
        if (playerInterfaceCanvas.gameObject.activeSelf)
        {
            GameplayManager manager = GameplayManager.Instance;
            pocketPlayer1.text = manager.pocketCurrentPlayer1.ToString();
            pocketPlayer2.text = manager.pocketCurrentPlayer2.ToString();
            pointsPlayer1.text = manager.totalScorePlayer1.ToString();
            pointsPlayer2.text = manager.totalScorePlayer2.ToString();
            moneyInGame.text = manager.moneyInGame.ToString();
            roundsLeft.text = manager.roundsLeft.ToString();

            betRiskyButton.interactable = (manager.pocketCurrentPlayer1 >= manager.riskyBetValue && manager.pocketCurrentPlayer2 >= manager.riskyBetValue);
            betAverageButton.interactable = (manager.pocketCurrentPlayer1 >= manager.averageBetValue && manager.pocketCurrentPlayer2 >= manager.averageBetValue);
            betSmallButton.interactable = (manager.pocketCurrentPlayer1 >= manager.smallBetValue && manager.pocketCurrentPlayer2 >= manager.smallBetValue);
        }
    }

    public void ButtonStart()
    {
        mainMenuCanvas.gameObject.SetActive(false);
        playerInterfaceCanvas.gameObject.SetActive(true);
        SetActiveAllBetButtons(true);
    }

    public void ButtonRules()
    {
        rulesPanel.gameObject.SetActive(true);
    }

    public void ButtonSettings()
    {
        settingsPanel.gameObject.SetActive(true);
    }

    public void ButtonExit()
    {
        exitPanel.gameObject.SetActive(true);
    }

    public void ButtonExitYes()
    {
        PlayerPrefs.SetFloat("Music volume", musicSlider.value);
        PlayerPrefs.SetFloat("Effects volume", effectsSlider.value);
        Application.Quit();
    }

    public void ButtonExitNo()
    {
        exitPanel.gameObject.SetActive(false);
    }

    public void ButtonReturnFromRulesPanel()
    {
        rulesPanel.gameObject.SetActive(false);
    }

    public void ButtonReturnFromSettingsPanel()
    {
        settingsPanel.gameObject.SetActive(false);
    }

    public void ButtonReturnFromGame()
    {
        winImage.gameObject.SetActive(false);
        loseImage.gameObject.SetActive(false);

        mainMenuCanvas.gameObject.SetActive(true);
        playerInterfaceCanvas.gameObject.SetActive(false);
        GameplayManager.Instance.ResetGameplayData();
        SetActiveAllBetButtons(true);
    }

    public void ButtonThrowDices()
    {
        GameplayManager.Instance.ThrowDices(GameplayManager.Instance.spawnpointsPlayer1);
        throwDicesButton.gameObject.SetActive(false);
    }

    public void PlaceBetSmall()
    {
        GameplayManager manager = GameplayManager.Instance;
        manager.SetBet(manager.smallBetValue);
        SetActiveAllBetButtons(false);
    }

    public void PlaceBetAverage()
    {
        GameplayManager manager = GameplayManager.Instance;
        manager.SetBet(manager.averageBetValue);
        SetActiveAllBetButtons(false);
    }

    public void PlaceBetRisky()
    {
        GameplayManager manager = GameplayManager.Instance;
        manager.SetBet(manager.riskyBetValue);
        SetActiveAllBetButtons(false);
    }

    private void SetActiveAllBetButtons(bool _isActive)
    {
        betSmallButton.gameObject.SetActive(_isActive);
        betAverageButton.gameObject.SetActive(_isActive);
        betRiskyButton.gameObject.SetActive(_isActive);

        throwDicesButton.gameObject.SetActive(!_isActive);
    }

    public void SoundtrackVolumeUpdate()
    {
        soundtrackBackgroundSource.volume = musicSlider.value / musicSlider.maxValue;
    }

    public void EffectsVolumeUpdate()
    {
        float currentValue = 0.5f * effectsSlider.value / effectsSlider.maxValue;
        foreach (AudioSource aSource in knockingDiceSources) aSource.volume = currentValue;
        knockingTestClip.volume = currentValue;
        knockingTestClip.Play();
    }
}
