﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceFace : MonoBehaviour
{
    /// <summary>
    /// A series of possible types of dice's faces that correspond to types of units.
    /// </summary>
    public enum Face
    {
        _blank,
        archer,
        horseman,
        pikeman,
        shielder,
        swordsman
    };

    /// <summary>
    /// A type of a dice's face.
    /// </summary>
    public Face myFace;
}
